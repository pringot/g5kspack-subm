# G5KSPACK Manual
## Concepts
The idea behind g5kspack is to ease the work with `spack` on platform such as Grid'5000, by:
- Allowing to work on the installation of spack packages on a compute node using a work copy, committing changes to production only when ok.
- Allowing to test packages installed in the work copy on several nodes (e.g. openmpi).
- Snapshotting the whole spack files hierarchy, so that diff and rollback are possible.
- Versionning the local spack configurations (otherwise ignored in the spack upstream branches) in a git branch.
- Versionning local changes to spack's packages.
- Tracking actions (installation, uninstallation) on packages along with the evolution of the spack git repositry itself (evolution of the spack tool and of packages).

g5kspack is made of a set of bash functions that use spack, git, ssh, ZFS and NFS underneath.

## g5kspack workflow
The g5kspack uses ZFS on a remote server (named the g5kspack server) and git (e.g. with the gitlab.inria.fr service). Operations are wrapped in the `g5kspack_*` functions defined in the `setup.sh` script.

### ZFS
The workflow uses ZFS with 2 datasets for a spack release, stored on the g5kspack server (several spack releases can be deployed completly independently, e.g. `develop` and `v.0.18`):
- A first dataset (the origin dataset) that holds the whole spack files hierarchy that is exported to all nodes via NFS in read-only, e.g.: `zdata/spack-develop` or `zdata/spack-v0.18`.
- A second dataset (the work dataset) that is temporarily created as a work copy of the first one (e.g. `zdata/spack-develop_work` or `zdata/spack-v0.18_work`) when working on the installation of spack packages, then deleted when work is over. The work copy is exported via NFS in read-write mode only to the node where the work is done, and in read-only to other nodes for mutli-node tests.

The first dataset is versionned using ZFS snapshots, which are automatically created for each work session.

All operations on the ZFS datasets on the g5kspack server are transparently triggered from the work node via ssh by the `g5kspack_*` functions.

The first dataset can be either exported to and mounted by all nodes directly to provide spack, or be rsynched to another NFS server (or possibly several NFS server in the case of a multi-sites platform such as Grid'5000).

### git
The workflow uses git with the following:
- A git repository that tracks the site installation of spack (i.e. the site configuration of spack and installation spack packages). Its remote name is `origin`.
- The official spack project repository at `https://github.com/spack/spack.git` accessed in read-only. Its remote name is `upstream` and can be used to ease mergers.
- The g5kspack project repository `git@gitlab.inria.fr:grid5000/g5kspack.git`, which provides the g5kspack tool (the `setup.sh` script) and is configured as a git submodule in the `g5kspack` sub-directory (also contains this README).
- One or more branches that follow (manual mergers when needed) the spack upstream branches/releases, e.g.:
  - `g5k/develop` (`origin/g5k/develop`) <- `develop` (`upstream/develop`)
  - `g5k/v0.18` (`origin/g5k/v0.18`) <- `releases/v0.18` (`upstream/releases/v0.18`)

Each `g5k/*` branch initially forks its corresponding upstream branch it follows. Then:
- A `g5kspack.conf` configuration file is added.
- Spack configuration files are committed to the branch to version them (in `etc/spack/*`).
- The `g5kspack` git sub-module is added.
It is deployed on the ZFS dataset during a first work session using the `g5kspack_init` function, which does all the initial work.

To sum up, the following actions are tracked in a `g5k/*` branch:
- Changes to the local spack configuration (`etc/spack/*`), done manually.
- Installation or uninstallation of packages with `g5kspack_install` and `g5kspack_uninstall`: `.spack/installation_environment.json` and `.spack/spec.json` files of the installed package are verionned, and the installation log is copied in the commit message as well as in an annoted git tag that uniquely marks the action on the package. Commit and tag are named after the package name and hash.
- Any local changes to spack packages installation scripts (cherry-pick from the upstream branch or local changes), done manually.
- Any merger of the upstream branch from time to time (done manually). Warning: it make no sense to rebase the branch on upstream.
- Possibly some arbitrary commands run with `g5kspack_run`.
- The g5kspack configuration file: `g5kspack.conf`.

Along with the git commits and pushs to the git origin remote, files are also pushed to the ZFS dataset on the g5kspack server. All that is taken care of by the `g5kspack_commit_work` function. Changes can only be committed to ZFS if the git status (local and remote) is clean.

## g5kspack configuration variables
The g5kspack functions use some shell variables for their configuration, that are set in the `g5kspack.conf` file and defined when the `setup.sh` file is sourced. The `g5kspack.conf` file can be located:
- Either in the current working directory (for the initial setup only).
- Or in the parent directory of the setup.sh script (e.g. `/grid5000/spack/develop/g5kspack.conf` with `/grid5000/spack/develop/g5kspack/setup.sh`)
Once `setup.sh` is sourced, the `$G5KSPACK_CONF` shell variable provides the path of the `g5kspack.conf` file that was used. To change the configuration (e.g. to change the spack branch), unset the variable before sourcing the new `setup.sh` file.

The g5kspack configuration variables are:
- `G5KSPACK_REMOTE` -> the remote URL of the git project that tracks the site's spack installation .
- `G5KSPACK_BRANCH` -> the git branch that tracks the site's spack installation (e.g. `g5k/develop`, `g5k/v0.18`).
- `G5KSPACK_UPSTREAM_REMOTE` -> official spack git remote URL (upstream).
- `G5KSPACK_UPSTREAM_RELEASE` -> spack release to use (e.g. `develop`, `v0.18`, ...).
- `G5KSPACK_TOOL_REMOTE` -> the g5kspack tool git repository URL (is not set, use `git@gitlab.inria.fr:grid5000/g5kspack.git`)
- `G5KSPACK_TOOL_RELEASE` -> the g5kspack tool release (is not set, use the `main` branch)
- `G5KSPACK_ROOT` -> spack mount point on nodes (e.g. `/grid5000/spack/develop`).
- `G5KSPACK_SERVER` -> g5kspack server (hosting the ZFS datasets).
- `G5KSPACK_SSHCMD` -> ssh command to connect as root to the g5kspack server.
- `G5KSPACK_NFSEXPORT` -> NFS share path for the work copy.
- `G5KSPACK_DATASET_ORIGIN` -> ZFS origin dataset name.
- `G5KSPACK_DATASET_WORK=` -> ZFS work copy dataset name.
- `G5KSPACK_DEFAULT_COMPILER` -> default compiler to use in spack.
- `G5KSPACK_DEFAULT_TARGET` -> default architecture target in spack.
- `G5KSPACK_NFS_SERVERS_SYNC_TRIGGER` -> trigger on the g5kspack server to sync to other NFS servers.
- `G5KSPACK_GIT_PUSH` -> configure weither to push on git remote or not.
- `G5KSPACK_NFSMOUNT_OPTS` -> provide additional NFS mount options
- `G5KSPACK_NFSEXPORT_OPTS` -> provide additional NFS export options

Other variables that should not be set in the configuration:
- `G5KSPACK_CONFIG` -> location of the `g5kspack.conf` file.
- `G5KSPACK_NON_INTERACTIVE` -> for scripted use of g5kspack: do not ask questions, use default answers.
- `G5KSPACK_NO_SPEC_BEFORE_INSTALL` -> do not show `spack spec` before `install` in `g5kspack_install`.
- `G5KSPACK_GIT_ALLOW_UNCLEAN` -> allow commit to ZFS with git status unclean.

## g5kspack basic usage
### Submit a job:
```
frontend$ oarsub -r now,19
```
This reserves a node from now to 19:00. Then we wait for the job to run.

### Connect to the job via ssh, and make sure to have you SSH key to access gitlab and the g5kspack server available:
```
workstation$ ssh dahu-32.grenoble.g5k -A
```
We use the `-A` switch to forward the SSH agent.

### Use sudo-g5k to become root preserving access to your SSH agent:
```
node$ sudo-g5k -sH --preserve-env=SSH_AUTH_SOCK
```
You may run `ssh-add -L` to check what keys are available via the SSH agant.

Note that all g5kspack commands are always run as root.

### Source the g5kspack functions
```
node# . /grid5000/spack/develop/g5kspack/setup.sh
```
Note that here we choose to work with spack `develop` branch.

### Setup the g5kspack work environment
```
node# g5kspack_begin_work
```
This creates the ZFS dataset `spack-develop_work` on the g5kspack server as a work copy (clone) of the `spack-develop` dataset, and exports it via NFS to the node as RW, and to other nodes as RO. A snapshop of the ZFS original dataset is also made.

Then, the work copy is mounted on the node in `/grid5000/spack/develop` on top of the original copy.

### Install some packages
```
node# g5kspack_install openmpi+atomics+cuda+cxx+singularity fabrics=cma,knem,ofi,psm2,ucx,verbs
```
Unless G5KSPACK_NO_SPEC is set, it shows the spec of what is to be installed and ask for a confirmation.

Then it installs `openmpi`. If succesful, it commits to git the spec and installation environment of the package, logging the installation command output. It also tags the commit with the package name and hash.

Finally it optionaly refreshs environment modules.

Note: 
- The `install_environment.json` file of the installed package, which is part of the commit, contains a `spack_version`. That versions gives the hash of the commit where the installation was done, which in fact is the parent of the commit `g5kspack_install` just created.
- The `spec.json` file gives explicitly all the parameters to reinstall the package identically. That file could be passed as argument `g5kspack_install` in case the package installation would have to be done again.

At this stage, other packages can be installed.

If anything seems to justify to cancel what was done:
* drop the work copy (`g5kspack_end_work`) and possibly start over (`g5kspack_begin_work`).
* in case you did push anything to the git origin remote already, see the rollback section below.

### Commit changes
If everything went fine, commit work to original copy of the ZFS dataset:
```
node# g5kspack_commit_work
```
This will first do some sanity checks.
Then refresh the environment modules.
Then show the diff in the spack files hierarchy.
Then push to git origin remote.
Finally copy (rsync) data from the work dataset to the original dataset and do a snapshot (ZFS).

Operation can continue with other `g5kspack_install` commands.

### End work
```
node# g5kspack_end_work
```
Once work is over, we request the ZFS work dataset to be unmounted on the node and destroyed on the g5kspack server.

## Other commands
### g5kspack_uninstall
```
node# g5kspack_uninstall /wfikewfdfddb32fwe
```
This uninstall a package that was installed with `g5kspack_install`. Trying to uninstall other packages with `g5kspack_uninstall` will fail. It then commit changes to git: removal of the spec and installation environment files from git. A git tag is also created, named after the uninstalled package name and tag.

### g5kspack_new_compiler_target
```
node# g5kspack_new_compiler_target gcc-11.3.0 power8le
```
This install a new spack arborescance for a compiler and target architecture, here gcc-10.3.0 and power8le. Note that this will result in 3 commits:
-1 compilation/installation of the compiler with the distribution compiler (e.g. Debian's gcc) (calls `g5kspack_install`).
-2 compilation/installation of the compiler with itself (e.g. Debian's gcc) (calls `g5kspack_install` again).
-3 commit the configuration related to the new compiler.

Note: It results in 3 differents git commits.

### g5kspack_run
Run a arbitrary command and log its stdout and stderr in a git commit. Some files could be added to the commit (afterward).

### g5kspack_take_over_work
Take over a previous work, possibly after the end of the provious job, and possibly on a new node. The work copy is re-exported RW to the current node.

### g5kspack_setup_other_nodes
Mounts (or umount if the first argument is `-u') the work copy on some other nodes. This allows for testing a package on many nodes.

### g5kspack_refresh_modules
Do refresh environment modules, if not done already (this command is also called by other functions).

### g5kspack_init
Only use initially (when the ZFS dataset is empty). This clones the `g5k/develop` branch from `origin` in the dataset and do some other minor stuff. Note that the `g5k/develop` branch may already contain some spack configurations (e.g. files in `etc/spack/`), but since this is the initialisation of the ZFS dataset, no commit/tag should refer to any package installation.

### g5kspack_tool_update
Fetch the latest version of the g5kspack tool, updating the git submodule that contains it (g5kspack sub-directory).

### g5kspack_additional_spack_repo
Allows to add and update an additional spack repository and track its versions in git submodule.

### g5kspack_rollback_git_origin
This command is meant to be called after a rollback of the ZFS dataset, or if commits/tags were erroneously pushed to origin.
It will roll back the HEAD of the $G5KSPACK_BRANCH on origin to its current local state in the ZFS dataset (using `git push -f`).
Then it will delete any git tag created by `g5kspack_install` or `g5kspack_uninstall` that became orphan after the branch rollback.

### g5kspack_trigger_NFS_servers_sync
In case a synchronisation mechanism is in place on the g5kspack server to deploy the spack file hierarchy to many NFS servers (mutli-site deployment), it immediately triggers it. If not triggered, synchronisation is usually done once a day (timer).

## Tweaks
### ADM_USER
Environment variable used to identify who is working. It has to be set manually if working on a deployed env.

### G5KSPACK_NON_INTERACTIVE
Environment variable to set to bypass interactivity, e.g. in unattended scripts.

### G5KSPACK_NO_SPEC_BEFORE_INSTALL
Environment variable to set to not show the spec of a package to install when calling `g5kspack_install`.

### G5KSPACK_CONFIG
Environment variable to locate the `g5kspack.conf` file.

## Initial setup and ZFS operations
Here we work with the `develop` branch, however you may substitute with `v0.18` for instance.

### Initial setup
On the g5kspack server.

Create a zfs pool:
```
nfs# zpool create zdata <storage dev>
nfs# zfs set mountpoint=none zdata
nfs# zfs set atime=off zdata
```
Note: `atime=off` is required so that snapshots do not grow when files are just accessed.

Create the spack dataset:
```
nfs# zfs create zdata/spack-develop -o mountpoint=/export/spack/develop -o sharenfs="ro=*,no_root_squash"
```
Or if you do not export via NFS the directory, but just rsync it to a dedicated NFS server, just do:
```
nfs# zfs create zdata/spack-develop -o mountpoint=/export/spack/develop
```

Nodes should now be able to mount `/export/spack/develop/` via NFS. But it is empty.

Please mind that the `g5k/develop` branch must pre-exist in the git repository, be it just a fork of the spack's develop branch, or already contains some commits with some configurations.
Also, as we are starting from scratch (without any package installation history), make sure the branch does not contain any commit or tag refering to former `g5kspack_install` or `g5kspack_uninstall call` of a previous use of g5kspack.

As explained above, we now create a job on a node, then connect to it by ssh, and become root with preserving the SSH agent:
```
node$ sudo-g5k -sH --preserve-env=SSH_AUTH_SOCK
```
(All g5kspack commands are always run as root.)

The next steps require the g5kspack functions to be available. They are defined in the `setup.sh` file, but it is not avaiblable for now: chicken-and-egg issue here, since the dataset is initially empty.

To overcome this, we have to temporarilly retrieve a copy of the `setup.sh` file from this git repositoy (https://gitlab.inria.fr/grid5000/g5kspack/-/blob/g5kspack/setup.sh). Furthermore, a `g5kspack.conf` configuration file must be crafted. A configuration could also be pulled from an existing spack git repository which already uses g5kspack, such as https://gitlab.inria.fr/grid5000/spack/-/blob/g5k/v1/g5kspack.conf, then adapated). Both files can be stored in a same temporary directory.

In the `g5kspack.conf` file, make sure `G5KSPACK_REMOTE` defines the git repository that will track you spack installation.

That `G5KSPACK_REMOTE` git repository must exists at this stage. It can be created empty (no branch), as `g5kspack_init` with handle the creation of the branch.

Once done, we can source `setup.sh` from the temporary directory so that the g5kspack functions are now defined!
```
node# . setup.sh
```
We can now begin work!
```
node# g5kspack_begin_work
```
This creates the work copy (ZFS dataset exported as `/export/spack/develop_work`) and mount it. It is initially empty. We then run:
```
node# g5kspack_init
```
This installs the g5kspack file hierarchy in the work copy (`$G5KSPACK_ROOT`), including a copy of the temporary `g5kspack.conf`. Starting from now, the installed copies of `g5kspack.conf` and `setup.sh` will be used. Temporary file can be deleted. Indeed, `g5kspack_init` ends by executing:
```
unset G5KSPACK_CONFIG
. /grid5000/spack/develop/g5kspack/setup.sh
```

We can now commit the work copy to the original dataset, with:
```
node# g5kspack_commit_work
```

And possibly end the work (destroy the work copy) with:
```
node# g5kspack_end_work
```
Then finally destroy your job.

### Rollback a former snapshot
On the g5kspack server.

To list snapshots (e.g. for the `develop` branch), do:
```
zfs list -t snapshot zdata/spack-develop
```
To check differences with a previous snapshot, do:
```
zfs diff zdata/spack-develop@2022-04-15_12:52:31 zdata/spack-develop
```
It is also possible to look at snapshots in the `/export/spack/develop/.zfs/snapshots/` subdirectory in the mount point of the dataset.

To rollback to a previous snapshot:
```
zfs rollback zdata/spack-develop@2022-04-15_12:52:31
```
At this stage, you will probably have to roll back changes pushed to git, with the `g5kspack_rollback_git_origin` command.

### Multi-site deployment
For a single site deployment, the g5kspack server can export the ZFS dataset by NFS to all nodes. But for a multi-site deployement, it is proposed to rsync the ZFS dataset mounted in `/export/spack/develop` to an NFS server on each sites. That NFS server is then mounted by all the nodes of the site (in read-only).

Synchronization from the g5kspack server to the NFS servers of all sites can done by a service running on g5kspack server.

To work on the spack package installation, any node from any site (e.g. if requiring a specific node only available in a specific site) can be used as long as it can mount the work copy (ZFS dataset) exported by NFS by the g5kspack server.

## Internal details
### Notes about snapshots and the cloned work dataset
ZFS snapshots are read-only. The work dataset `spack-develop_work` is thus a clone of the latest snapshot. A clone is read-write.

A clone can be promoted, which swaps it with the origin dataset (make it become the origin, and the origin become a clone). Once promoted, the clone could be renamed to replace the origin, becoming the new `spack-develop` dataset. However this breaks the NFS share: NFS clients will loose the NFS mount because the export changed. Thus, this technic is not valid to commit work done in the work dataset to the origin dataset.

As a result, `g5kspack_commit_work` does a `rsync` from the work dataset to the origin dataset.
